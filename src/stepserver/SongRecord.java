/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stepserver;

/**
 *
 * @author v1toko
 */
public class SongRecord {
    
    private int maxRecord;
    private String first;
    private String second;
    private String third;
    
    public SongRecord()
    {
        maxRecord = 0;
        first = "(none)";
        second = "(none)";
        third = "(none)";
    }

    /**
     * @return the maxRecord
     */
    public int getMaxRecord() {
        return maxRecord;
    }

    /**
     * @param maxRecord the maxRecord to set
     */
    public void setMaxRecord(int maxRecord) {
        this.maxRecord = maxRecord;
    }

    /**
     * @return the first
     */
    public String getFirst() {
        return first;
    }

    /**
     * @param first the first to set
     */
    public void setFirst(String first) {
        this.first = first;
    }

    /**
     * @return the second
     */
    public String getSecond() {
        return second;
    }

    /**
     * @param second the second to set
     */
    public void setSecond(String second) {
        this.second = second;
    }

    /**
     * @return the third
     */
    public String getThird() {
        return third;
    }

    /**
     * @param third the third to set
     */
    public void setThird(String third) {
        this.third = third;
    }
}
