package stepserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * Clase que maneja los paquetes que envia el cliente. También se encarga de
 * enviar las respuesta a él.
 *
 * @version 2.0.1, 06-01-2016
 * @author Víctor Gajardo Henríquez (v1toko)
 */
public class Client implements Runnable {    

    /**
     * Crea un cliente y lo ingresa al server.
     *
     * @param client Es la conexión recibida desde main.
     */
    Client(Socket client) {
        m_Connection = client;        
        m_bConnected = true;
        SetStreams();

        //cada cliente tiene su pinger
        Thread pinger = new Thread(() -> {
            do {
                PingClient();
                
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while (m_bConnected);
            System.out.println("Stop pinger for " + m_sUsername);
        }, "Pinger");
        pinger.start();
    }

    /**
     * Lee paquetes con formato de SMO
     *
     * @param bData Información leida desde el paquete
     */
    public void SMOParseData(byte[] bData) {
        String str = new String(bData);
        //System.out.println(str);

        //empezamos leyendo desde 1, puesto que el 1 es el comando
        int iSMOCommand = (int) bData[1];
        switch (iSMOCommand) {
            case 0: //login
            {
                //m_iPlayerNumber = (int) bData[2];
                Integer iPlayerNumber = (int) bData[2];
                int iHash = (int) bData[3]; //0 md5, 1 md5 + salt
                int iLastPos = GetStringLastPos(bData, 4);
                m_sUsername = GetStringNT(bData, 4);
                String sPass = GetStringNT(bData, iLastPos + 1);

                //0 is approved
                System.out.println("PASS: " + sPass);

                Player p = Player.getLogin(m_sUsername, sPass);
                if (p == null) {
                    PutBytes(PacketUtils.LoginResponse("Bad username or password for player" + m_sUsername, 1, 0, iPlayerNumber));
                    System.out.println("Bad username or password for player" + m_sUsername);
                } else {
                    p.setPlayerNumber(iPlayerNumber);
                    if (m_Player1 == null || m_Player1.getUsername().equalsIgnoreCase(m_sUsername)) {
                        m_Player1 = p;
                        m_Player1.setUsername(m_sUsername);
                        m_Player1.setPassword(sPass);
                        PutBytes(PacketUtils.LoginResponse("Welcome to the server " + m_sUsername, 0, p.getPlayerId(), iPlayerNumber));
                        System.out.println("Welcome to the server " + m_sUsername);
                    } else if (m_Player1 != null && m_Player2 == null) {
                        m_Player2 = p;
                        m_Player2.setUsername(m_sUsername);
                        m_Player2.setPassword(sPass);
                        PutBytes(PacketUtils.LoginResponse("Welcome to the server " + m_sUsername, 0, p.getPlayerId(), iPlayerNumber));
                        System.out.println("Welcome to the server " + m_sUsername);
                    }
                }

                Send();
                break;
            }
            case 1: {
                
            }

            //Send();
            break;
            case 2: {
                
            }

            //Send();
            break;
            
            //Save last song score (only in rank)
            case 3: {
                ByteBuffer bb = ByteBuffer.wrap(bData, 2, 1000);
                Integer iMode = (int) bb.get();
                Integer iMeter = (int) bb.get();
                Integer iPlayerID = bb.getInt();
                Integer iPerfects = bb.getInt();
                Integer iGreats = bb.getInt();
                Integer iGoods = bb.getInt();
                Integer iBads = bb.getInt();
                Integer iMisses = bb.getInt();
                Integer iCombo = bb.getInt();
                Integer iScore = bb.getInt();
                String sMd5 = GetStringNT(bData, 36);

                //System.out.println("md5 to save: " + sMd5);
                Song.saveSongRecord(iMode, iMeter, iPlayerID, iPerfects, iGreats, iGoods, iBads, iMisses, iCombo, iScore, sMd5);
            }
            break;
            
            //Send logout
            case 4: {
                ByteBuffer bb = ByteBuffer.wrap(bData, 2, 1000);
                Integer iPlayerID = bb.getInt();
                Integer iPlayerNumber = null;

                if (m_Player1 != null) {
                    if (Objects.equals(m_Player1.getPlayerId(), iPlayerID)) {
                        iPlayerNumber = m_Player1.getPlayerNumber();
                        m_Player1 = null;
                    }
                }

                if (m_Player2 != null) {
                    if (Objects.equals(m_Player2.getPlayerId(), iPlayerID)) {
                        iPlayerNumber = m_Player2.getPlayerNumber();
                        m_Player2 = null;
                    }
                }

                //enviamos solo si el playernumber esta correcto
                if (iPlayerNumber != null) {
                    PutBytes(PacketUtils.SendLogout(m_iPlayerNumber, iPlayerID));
                    Send();
                }
            }
            break;
            case 5: {

            }
            break;
            default:
                Logger.Trace("Commando SMO desconocido: " + iSMOCommand);
        }
    }    

    /**
     * Comienza la ejecución del thread
     */
    public void run() {
        do {
            Update();
            try {
                Thread.sleep(60);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (m_bConnected);
    }    
    
    //socket que mantiene la conexión
    Socket m_Connection;
    //server usa este stream para leer lo que manda el cliente
    DataInputStream m_ToReceivePacket;
    //Server usa este stream para mandar al cliente paquetes
    DataOutputStream m_ToSendPacket;

    //nos dice si está conectado
    boolean m_bConnected = false;
    //0 primer player, 1 segundo player (en el pc que se juega, no en la room)
    int m_iPlayerNumber = 0;
    //Identifica si el usuario ha empezado la sincronización.
    //0 no sincronizado
    //1 sincronizado y listo para empezar a refrescar los scores
    //2 refrescando scores
    int m_iSynchronized = 0;

    //nombre de usuario del cliente
    String m_sUsername;
    /*
     * Status: 0	Inative (no info on this user yet) 1	Active (you know who it
     * is) 2	In Selection Screen 3	In Options 4 In Evaluation
     */
    int m_iState = 1;

    /*
     * 0: exited ScreenNetSelectMusic 1: entered ScreenNetSelectMusic 2: **Not*
     * Sent** 3: entered options screen 4: exited the evaluation screen 5:
     * entered evaluation screen 6: exited ScreenNetRoom 7: entered
     * ScreenNetRoom
     */
    int m_iAction = 0;

    //Cuando un player crea una room guardamos el nombre, si es lobby no lo guardamos
    String m_sClientRoomName = "";

    //el creador de una room puede elegir cancion, el resto no
    boolean m_bRoomAdmin = true;

    //guarda el color que usará el usuario en el chat
    String m_sChatColor = "";

    //dice si el cliente es "lacker"
    boolean m_bLacker = false;

    //Version del protocolo (sirve para los scores)
    int m_iProtocol = 0;

    //Identifica el nombre del build del sm que se conectó
    String m_sBuildName = "";

    //guarda los datos del gameplay del player por cada stage individual
    //cuando se juega una nueva stage, se hacen 0
    PlayerStageStats m_PlayerStats = this.new PlayerStageStats();
    Player m_Player1;
    Player m_Player2;

    //identifica los style
    enum Style {

        Style_Single,
        Style_HalfDouble,
        Style_Double,
        Style_Couple
    }

    //stepmania enum int's que se leen de los packets
    final int SINGLE = 5;
    final int HDOUBLE = 6;
    final int DOUBLE = 7;
    final int COUPLE = 8; //sin uso pero hay q definir igual
    //identifica los styles

    enum Difficulty {

        Difficulty_Beginner,
        Difficulty_Easy,
        Difficulty_Medium,
        Difficulty_Hard,
        Difficulty_Challenge,
        Difficulty_Edit,
        Difficulty_None,
    }
    //guardan el style, dificultad y nivel respectivamente
    Style m_Style = Style.Style_Single;
    Difficulty m_Difficulty = Difficulty.Difficulty_None;
    int m_iMeter = 0;
    //los dejamos como ints para compatibilizar el networking
    int m_iStyle = 0;
    int m_iDifficulty = 0;
    //Identifica los scores de los steps

    enum TapScore {

        ST_INVALID,
        ST_MINEHIT,
        ST_MINEMISSED,
        ST_MISS,
        ST_W5,//boo
        ST_W4,//good
        ST_W3,//great
        ST_W2,//perfect
        ST_W1,//marv
        ST_LETGO,
        ST_HELD, //ok
        ST_NUMSTEPTYPES
    };
    //identifica el estado de la round

    enum RoundState {

        State_Invalid,
        State_Active,
        State_Ended
    };
    //Guarda los scores por cada stage individualmente

    private class PlayerStageStats {

        int m_iScore;
        int m_iLife; // 0 - 140 in stepnxa
        int m_iMaxCombo;
        int m_iCurCombo;
        int m_iGrade; //sm equivalent enum
        int m_iPlayerID;
        int m_iDifficulty;
        int m_iLevel;
        String m_sOptions;
        int[] TapScores;
        String m_sSongHash; //unused until the server has changed system
        private RoundState m_RoundState = RoundState.State_Invalid;

        public PlayerStageStats() {
            this.m_iScore = 0;
            this.m_iMaxCombo = 0;
            this.m_iLife = 0;
            this.m_iCurCombo = 0;
            this.m_iDifficulty = 0;
            this.m_iGrade = 0;
            this.m_iLevel = 0;
            this.m_iPlayerID = 0;
            this.m_sOptions = "";
            TapScores = new int[10];
        }

        public void UpdateStats(int iScore, int iLife, int iMaxCombo, TapScore score, float fOffset, int iGrade) {
            this.m_iScore = iScore;
            this.m_iLife = iLife;
            this.m_iGrade = iGrade;
            if (iMaxCombo > this.m_iMaxCombo) {
                this.m_iMaxCombo = iMaxCombo;
            }
            this.m_iCurCombo = iMaxCombo;
            switch (score) {
                case ST_W1:
                case ST_W2:
                    TapScores[8]++;
                    break;
                case ST_W3:
                    TapScores[6]++;
                    break;
                case ST_W4:
                    TapScores[5]++;
                    break;
                case ST_W5:
                    TapScores[4]++;
                    break;
                case ST_MISS:
                    TapScores[3]++;
                    break;
                default:
                    break;
            }
            /*
             * for(int i = 0; i < TapScores.length; i++) {
             * System.out.println("TapScore[" + i + "] total: " + TapScores[i]);
             * }
             */
        }

        public void SetPlayerInfo(int iPlayerID, int iDiff, int iLevel, String sOptions) {
            this.m_iPlayerID = iPlayerID;
            this.m_iDifficulty = iDiff;
            this.m_iLevel = iLevel;
            this.m_sOptions = sOptions;
        }

        public void SetPlayerID(int iID) {
            this.m_iPlayerID = iID;
        }

        /**
         * @return the m_RoundState
         */
        public RoundState getRoundState() {
            return m_RoundState;
        }

        /**
         * @param m_RoundState the m_RoundState to set
         */
        public void setRoundState(RoundState m_RoundState) {
            this.m_RoundState = m_RoundState;
        }
    }

    /**
     * Responde al comando recibido desde el cliente
     *
     * @param iCommand Comando leido al que hay que responder
     * @param bData Información leida desde el paquete
     */
    public void Response(int iCommand, byte[] bData) {
        //como el comando 1 es el ping, no respondemos a el.
        if (iCommand != 1) {
            System.out.println("Responsing to command: " + iCommand);
        }
        switch (iCommand) {
            case 2: //hello client = 002 | server = 130
            {
                int iProtocol = bData[1];
                String sBuild = GetStringNT(bData, 2);
                int iLastPos = GetStringLastPos(bData, 2);
                String sMd5 = GetStringNT(bData, iLastPos + 1);

                this.m_iProtocol = iProtocol;
                this.m_sBuildName = sBuild;

                System.out.println("Protocol: " + m_iProtocol + " SM version: " + m_sBuildName);
                System.out.println("MD5: " + sMd5);

                /*if (!Executable.testExecutableMD5(sMd5)) {
                    System.out.println("Tu exe no es valido.");
                    Disconnect();
                    return;
                }*/
                PutBytes(PacketUtils.Hello("StepServer"));
                Send();
            }
            break;
            case 12: //smoparse data
            {
                //
                //  Primero, leer el paquete
                //
                SMOParseData(bData);
            }
            break;
            default: {
            }
        }
    }

    /**
     * Asigna los streams de la conexión.
     */
    protected void SetStreams() {
        try {
            m_ToReceivePacket = new DataInputStream(m_Connection.getInputStream());
            m_ToSendPacket = new DataOutputStream(m_Connection.getOutputStream());
        } catch (Exception ex) {
            Logger.Trace("Client::SetStreams()");
            ex.printStackTrace();
        }
    }

    /**
     * Actualiza y lee cuando llegan nuevos paquetes.
     */
    public void Update() {
        byte[] buf = new byte[1020];
        try {
            //si no hay bytes listos para leer salimos.
            //esto bloquea el thread.
            if (m_ToReceivePacket.available() <= 0) {
                return;
            }

            //skip primero 4 bytes (tamaño del packet, ezsockets)
            m_ToReceivePacket.skipBytes(4);
            int iBytesReads = m_ToReceivePacket.read(buf);
            //System.out.println("Bytes readed: " + iBytesReads);
            //ParseData(buf);
            if (iBytesReads != 0) {
                int iCommand = (int) buf[0];
                Response(iCommand, buf);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Desconecta el cliente del server.
     */
    public void Disconnect() {
        Logger.Trace("Client::Disconnect()");
        try {
            m_ToReceivePacket.close();
            m_ToSendPacket.close();
            m_Connection.close();
            m_bConnected = false;
        } catch (Exception e) {
            e.printStackTrace();
            Logger.Trace(toString() + "::Disconnect() exception");
        }
    }

    /**
     * Envia un ping al cliente
     */
    public void PingClient() {
        try {
            //actualmente esto no tiene efecto cuando el cliente se desconecta sorpresivamente
            if (!this.m_Connection.isInputShutdown() && !this.m_Connection.isOutputShutdown()) {
                //m_ToSendPacket.write(PacketUtils.Ping());
                //m_ToSendPacket.flush();
            }
        } catch (Exception e) {
            Disconnect();
            //e.printStackTrace();
        }
    }

    /**
     * Manda un mensaje de sistema al cliente
     *
     * @param sMessage Mensaje a enviar al cliente
     */
    public void SystemMessage(String sMessage) {
        PutBytes(PacketUtils.SystemMessage(sMessage));
        Send();
    }

    /**
     * Envia un mensaje al chat del cliente
     *
     * @param sChat Mensaje a enviar al cliente
     * @param sOwner El nombre del usuario que envió el mensaje
     */
    public void SendChat(String sChat, Client Owner) {
        String sFullChat = Owner.m_sChatColor + Owner.m_sUsername + " |c0ffffff: " + sChat;
        PutBytes(PacketUtils.ChatMessage(sFullChat));
        Send();
    }

    /**
     * Envia un mensaje de algún evento a los usuario (usualmente)
     *
     * @param sChat Mensaje a enviar al cliente
     */
    public void SendServerChat(String sChat) {
        String sFullChat = "|c0ffffff " + sChat;
        /*
         * 007:(135)Chat Message Desc:	Add a chat message to the chat window on
         * some StepMania screens. Payload: Size	Description NT	Message
         */
        int iSize = 0;
        iSize = sFullChat.length() + 1;
        iSize++;
        Write4b(iSize);//size!
        Write1b(135);//server command
        WriteString(sFullChat.concat("\0"));
        Send();
    }

    /**
     * Lee desde from hasta que encuentra un 0 en data
     *
     * @param data información leida del paquete
     * @param from indica desde donde va a leer la información
     * @return devuelve la ultima posición leida.
     */
    public int GetStringLastPos(byte[] data, int from) {
        int i = from;
        while (i < 1020 && ((char) data[i] != 0)) {
            i++;
        }

        return i;
    }

    /**
     * Lee desde from hasta que encuentra un 0 en data
     *
     * @param data Información leida desde el paquete
     * @param from indica desde donde va a leer la información
     * @return devuelve el string que lee desde data
     */
    public String GetStringNT(byte[] data, int from) {
        int i = from;
        String sOut = "";
        while (i < 1020 && ((char) data[i] != '\0')) {
            sOut += String.valueOf((char) data[i]);
            i++;
        }

        return sOut;
    }

    /**
     * Envia la información del paquete al cliente
     */
    public void Send() {
        try {
            m_ToSendPacket.flush();
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }

    public void PutBytes(byte[] b) {
        try {
            m_ToSendPacket.write(b);
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }

    /**
     * Escribe en el Stream 1 byte
     *
     * @param b Información que se enviará al cliente
     */
    public void Write1b(int b) {
        byte a = (byte) b;
        try {
            m_ToSendPacket.writeByte(a);
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }

    /**
     * Escribe en el Stream 2 bytes
     *
     * @param b Información que se enviará al cliente
     */
    public void Write2b(int b) {
        short a = (short) b;
        try {
            m_ToSendPacket.writeShort(a);
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }

    /**
     * Escribe en el Stream 4 byte
     *
     * @param b Información que se enviará al cliente
     */
    public void Write4b(int b) {
        try {
            m_ToSendPacket.writeInt(b);
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }

    /**
     * Escribe en el Stream s.Length() bytes
     *
     * @param s Información que se enviará al cliente
     */
    public void WriteString(String s) {
        //System.out.println("String: " + s + " size " + s.length());
        try {
            m_ToSendPacket.writeBytes(s);
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }
}
