package stepserver;

import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @version 2.0.1, 06-01-2016
 * @author Víctor Gajardo Henríquez (v1toko)
 */
public class StepServer {

    //puerto en el que se bindea el socket
    static final int PUERTO = 8765;

    /**
     * Inicia el programa y los threads, también deja a la escucha el socket y
     * agrega los clientes
     */
    public void go() {
        try {
            ServerSocket serverSocket = new ServerSocket(PUERTO);

            if (MysqlConnect.getDbCon().conn.isClosed()) {
                System.exit(1);
            }

            while (true) {
                System.out.println("Listening for connections on port " + PUERTO + "... ");
                Socket client = serverSocket.accept();
                client.setSoTimeout(2000);
                Thread t = new Thread(new Client(client));
                t.start();
                System.out.println("Connected - " + client.getInetAddress());
                Thread.sleep(20);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            MysqlConnect.getDbCon().conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Método de entrada al programa.
     *
     * @param args Argumentos de entrada al programa
     */
    public static void main(String[] args) {
        StepServer server = new StepServer();
        server.go();
    }

}
