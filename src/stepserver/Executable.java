package stepserver;

import java.sql.ResultSet;

/**
 *
 * @version 2.0.1, 06-01-2016
 * @author Víctor Gajardo Henríquez (v1toko)
 */
public class Executable {

    private String md5;

    public Executable() {
    }

    public Executable(String md5) {
        this.md5 = md5;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public static boolean testExecutableMD5(String sMd5) {
        ResultSet rs = MysqlConnect.getDbCon().query("select md5 from executable where md5 = '" + sMd5 + "'");        
        try {
            if( rs != null && rs.next() )
                return true;
        } catch (Exception e) {
            e.printStackTrace();            
        }
        
        return false;
    }
}
