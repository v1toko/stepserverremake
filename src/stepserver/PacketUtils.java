package stepserver;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * @version 2.0.1, 06-01-2016
 * @author Víctor Gajardo Henríquez (v1toko)
 */
public final class PacketUtils {
    
    private static final int SIZEOF_BYTE = 1;
    private static final int SIZEOF_SHORT = 2;
    private static final int SIZEOF_INT = 4;
    private static final int SIZEOF_LONG = 8;

    private static final byte SERVER_VERSION = (byte)255;

    //server commands
    private static final byte CMD_PING = (byte)128;
    private static final byte CMD_HELLO = (byte)130;
    private static final byte CMD_ALLOWSTART = (byte)131;
    private static final byte CMD_GAMEOVERSTATS = (byte)132;
    private static final byte CMD_SCOREBOARDUPDATE = (byte)133;
    private static final byte CMD_SYSTEMMESSAGE = (byte)134;
    private static final byte CMD_CHAT = (byte)135;
    private static final byte CMD_STARTSELECTSONG = (byte)136;
    private static final byte CMD_UPDATEUSERLIST = (byte)137;
    private static final byte CMD_FORCESCREEN = (byte)138;
    private static final byte CMD_RESERVED = (byte)139;
    private static final byte CMD_SMOPACKET = (byte)140;
    private static final byte CMD_UDPPACKET = (byte)141;
    private static final byte CMD_ATTACKCLIENT = (byte)142;
    private static final byte CMD_XMLREPLY = (byte)143;
    private static final byte CMD_RANKEDSONG = (byte)144;
    private static final byte CMD_STYLEINFO = (byte)146;
    //extra commands
    private static final byte CMD_SMO_LOGIN = (byte)0;
    private static final byte CMD_SMO_RANKRECORDS = (byte)1;
    private static final byte CMD_SMO_STEPRECORD = (byte)2;
    private static final byte CMD_SMO_LOGOUT = (byte)4;
    private static final byte CMD_SMO_ALLRECORDS = (byte)8;
    //private static final byte CMD_SMO_UPDATEROOMS = (byte)1;

    public static byte[] LoginResponse(String sResponse, int approval, int playerid, int playernumber)
    {
        sResponse = sResponse.concat("\0");
        //packet size + command + smocmd + aproval status + playernumber + player_id + login response length
        int iCapacity = SIZEOF_INT + (SIZEOF_BYTE * 4) + SIZEOF_INT + sResponse.length();

        ByteBuffer buf = ByteBuffer.allocate(iCapacity);
        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_SMOPACKET);
        buf.put(CMD_SMO_LOGIN);
        buf.put((byte)approval);
        buf.put((byte)playernumber);
        buf.putInt(playerid);
        buf.put(sResponse.getBytes());

        return buf.array();
    }    
    
    public static byte[] SendRecords(SongRecord sr)
    {        
        String first = sr.getFirst().concat("\0");
        String second = sr.getSecond().concat("\0");
        String third = sr.getThird().concat("\0");
        
        //             packet size + command     + smocmd         + records +   max-score +   1st           +  2nd           +   3rd
        int iCapacity = SIZEOF_INT + SIZEOF_BYTE + SIZEOF_BYTE + SIZEOF_BYTE + SIZEOF_INT + first.length() + second.length() + third.length();

        ByteBuffer buf = ByteBuffer.allocate(iCapacity);
        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_SMOPACKET);
        buf.put(CMD_SMO_RANKRECORDS);
        buf.put((byte)3);
        buf.putInt(sr.getMaxRecord());
        buf.put(first.getBytes());
        buf.put(second.getBytes());
        buf.put(third.getBytes());

        return buf.array();
    }
    
    public static byte[] SendTopStepRecord(SongRecord sr, Integer PlayerNumber)
    {        
        String first = sr.getFirst().concat("\0");
        
        //             packet size + command     + smocmd      +  playernumber + max-score +   1st
        int iCapacity = SIZEOF_INT + SIZEOF_BYTE + SIZEOF_BYTE + SIZEOF_BYTE + SIZEOF_INT + first.length();

        ByteBuffer buf = ByteBuffer.allocate(iCapacity);
        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_SMOPACKET);
        buf.put(CMD_SMO_STEPRECORD);
        buf.put(PlayerNumber.byteValue());
        buf.putInt(sr.getMaxRecord());
        buf.put(first.getBytes());

        return buf.array();
    }
    
    public static byte[] SendAllRecords()
    {        
        int initialSize = 0;        
        ArrayList<Song> _songs = Song.getAllSongs();
        HashMap<String, SongRecord> _allSongRecords = new HashMap();
        for(Song _s : _songs) {
            SongRecord _sr = Song.getRecords(_s.getMd5());
            String first = _sr.getFirst().concat("\0");
            String second = _sr.getSecond().concat("\0");
            String third = _sr.getThird().concat("\0");
            //                records +   max-score +   1st           +  2nd           +   3rd
            initialSize += (SIZEOF_BYTE + SIZEOF_INT + first.length() + second.length() + third.length());
            _allSongRecords.put(_s.getMd5(), _sr);
        }
        
        //             packet size + command     + smocmd      +  playernumber + max-score +   1st
        int iCapacity = SIZEOF_INT + SIZEOF_BYTE + SIZEOF_BYTE + initialSize;

        ByteBuffer buf = ByteBuffer.allocate(iCapacity);
        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_SMOPACKET);
        buf.put(CMD_SMO_ALLRECORDS);
        
        Set<String> strmd5 = _allSongRecords.keySet();
        Iterator<String> _it = strmd5.iterator();
        while(_it.hasNext()) {
            SongRecord _sr = _allSongRecords.get(_it.next());
            String first = _sr.getFirst().concat("\0");
            String second = _sr.getSecond().concat("\0");
            String third = _sr.getThird().concat("\0");
            buf.put((byte)3);
            buf.putInt(_sr.getMaxRecord());
            buf.put(first.getBytes());
            buf.put(second.getBytes());
            buf.put(third.getBytes());
        }

        return buf.array();
    }
    
    public static byte[] SendLogout(Integer PlayerNumber, Integer PlayerID)
    {                
        //             packet size + command     + smocmd      +  playernumber + playerid
        int iCapacity = SIZEOF_INT + SIZEOF_BYTE + SIZEOF_BYTE + SIZEOF_BYTE + SIZEOF_INT;

        ByteBuffer buf = ByteBuffer.allocate(iCapacity);
        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_SMOPACKET);
        buf.put(CMD_SMO_STEPRECORD);
        buf.put(PlayerNumber.byteValue());
        buf.putInt(PlayerID);

        return buf.array();
    }

    public static byte[] ChatMessage(String sMessage)
    {
        sMessage = sMessage.concat("\0");
        int iCapacity = SIZEOF_INT + SIZEOF_BYTE + sMessage.length();
        ByteBuffer buf = ByteBuffer.allocate(iCapacity);

        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_CHAT);
        buf.put(sMessage.getBytes());

        return buf.array();
    }

    public static byte[] SystemMessage(String sMessage)
    {
        sMessage = sMessage.concat("\0");
        int iCapacity = SIZEOF_INT + SIZEOF_BYTE + sMessage.length();
        ByteBuffer buf = ByteBuffer.allocate(iCapacity);

        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_SYSTEMMESSAGE);
        buf.put(sMessage.getBytes());

        return buf.array();
    }

    public static byte[] Hello(String sServerName)
    {
        sServerName = sServerName.concat("\0");
        //packet size and info
        //string.length() + 1 for Null terminate
        int iCapacity = SIZEOF_INT + (2 * SIZEOF_BYTE) + (sServerName.length());
        ByteBuffer buf = ByteBuffer.allocate(iCapacity);

        buf.putInt(iCapacity-SIZEOF_INT); //restamos el packet size
        buf.put(CMD_HELLO);
        buf.put(SERVER_VERSION);
        buf.put(sServerName.getBytes());

        return buf.array();
    }

    public static byte[] Ping()
    {
        //packet size and info
        int iCapacity = SIZEOF_INT + SIZEOF_BYTE;
        ByteBuffer buf = ByteBuffer.allocate(iCapacity);

        buf.putInt(iCapacity - SIZEOF_INT);
        buf.put(CMD_PING);

        return buf.array();
    }    

}
