package stepserver;

import java.sql.ResultSet;

/**
 * @version 2.0.1, 06-01-2016
 * @author Víctor Gajardo Henríquez (v1toko)
 */
public class Player {
    
    private Integer playerId;    
    private String username;    
    private String password;    
    private Integer playerNumber;
    
    public static Player getLogin(String sUser, String sPass) {
        ResultSet rs = MysqlConnect.getDbCon().query("select * from player where username = '"+sUser+"' and password = '" + sPass.toUpperCase() + "' ");        
        Player p = new Player();
        try {
            if( rs != null && rs.next() )
            {                
                p.setPlayerId(rs.getInt("player_id"));
                p.setUsername(rs.getString("username"));
                return p;
            }
        } catch (Exception e) {
            e.printStackTrace();            
        }
        
        return null;
    }

    public Player() {
    }

    public Player(Integer playerId) {
        this.playerId = playerId;
    }

    public Player(Integer playerId, String username, String password) {
        this.playerId = playerId;
        this.username = username;
        this.password = password;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }      

    /**
     * @return the playerNumber
     */
    public Integer getPlayerNumber() {
        return playerNumber;
    }

    /**
     * @param playerNumber the playerNumber to set
     */
    public void setPlayerNumber(Integer playerNumber) {
        this.playerNumber = playerNumber;
    }
}
