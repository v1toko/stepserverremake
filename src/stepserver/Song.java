package stepserver;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author v1toko
 */
public class Song {
    
    private String md5;

    /* rank stuff, get the 3 top records */
    public static SongRecord getTopRecord(String md5File, Integer meter, Integer style) {
        SongRecord sr = new SongRecord();
        try {
            try (PreparedStatement pstmt = MysqlConnect.getDbCon().conn.prepareStatement("  select player.username, round.score \n"
                    + " from round, player, song, player_round\n"
                    + " where \n"
                    + " player.player_id = player_round.player_id\n"
                    + " and round.round_id  = player_round.round_id \n"
                    + " and round.song_id = song.song_id \n"
                    + " and round.meter = ?\n"
                    + " and round.style_id = ?\n"
                    + " and song.md5 = ? \n"
                    + " order by round.score desc \n"
                    + " limit 1")) {
                pstmt.setInt(1, meter);
                pstmt.setInt(2, style);
                pstmt.setString(3, md5File);
                try (ResultSet rs = pstmt.executeQuery()) {
                    while (rs != null && rs.next()) {
                        sr.setFirst(rs.getString(1));
                        sr.setMaxRecord(rs.getInt(2));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sr;
    }

    /* rank stuff, get the 3 top records */
    public static SongRecord getRecords(String md5File) {
        SongRecord sr = new SongRecord();
        try {
            try (PreparedStatement pstmt = MysqlConnect.getDbCon().conn.prepareStatement("select player.username, round.score \n"
                    + "from round, player, song, player_round\n"
                    + "where\n"
                    + "round.round_id  = player_round.round_id \n"
                    + "and round.song_id = song.song_id \n"
                    + "and song.md5 = ? \n"
                    + "order by round.score desc \n"
                    + "limit 0,3")) {
                pstmt.setString(1, md5File);
                try (ResultSet rs = pstmt.executeQuery()) {
                    int iCont = 0;
                    while (rs != null && rs.next()) {
                        switch (iCont) {
                            case 0:
                                sr.setFirst(rs.getString(1));
                                sr.setMaxRecord(rs.getInt(2));
                                break;
                            case 1:
                                sr.setSecond(rs.getString(1));
                                break;
                            case 2:
                                sr.setThird(rs.getString(1));
                                break;
                            default:
                                break;
                        }
                        iCont++;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sr;
    }

    public static void saveSongRecord(int mode, int lvl, int player, int pfc, int grt, int good, int bad, int miss, int combo, int score, String md5) {
        //INSERT INTO stepserver.round
        //(perfects, greats, goods, bads, miss, style_id, score, meter, maxcombo, song_id)
        //VALUES(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        MysqlConnect.getDbCon().insert(
                ""
                + " insert into round "
                + " (perfects,greats,goods,bads,miss,style_id,score,meter,maxcombo,song_id) "
                + " select "
                + pfc
                + "," + grt + "," + good + "," + bad + "," + miss + "," + mode + "," + score + "," + lvl + "," + combo
                + ",song_id from song where md5 = '" + md5.trim() + "'");
        
        Integer lastId = 0;
        try {
            try (PreparedStatement pstmt = MysqlConnect.getDbCon().conn.prepareStatement("SELECT LAST_INSERT_ID()");
                    ResultSet rs = pstmt.executeQuery()) {
                while (rs != null && rs.next()) {
                    lastId = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error al rescatar last round id");
        }
        
        if(lastId > 0) {
            MysqlConnect.getDbCon().insert("INSERT INTO stepserver.player_round(`Date`, round_id, player_id) VALUES(now(), " + lastId + ", " + player + ")");
        } else {
            System.out.println("la ultima ronda no se pudo recuperar");
        }
    }

    public static ArrayList<Song> getAllSongs() {
        ArrayList<Song> _songs = new ArrayList();
        try {
            try (PreparedStatement pstmt = MysqlConnect.getDbCon().conn.prepareStatement("select\n"
                    + "	song.md5\n"
                    + "from\n"
                    + "	song\n"
                    + "where\n"
                    + "	md5 is not null\n"
                    + "	and trim(md5) <> ''");ResultSet rs = pstmt.executeQuery()) {
                while (rs != null && rs.next()) {
                    Song _s = new Song();
                    _s.setMd5(rs.getString("md5"));
                    _songs.add(_s);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return _songs;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }
}
